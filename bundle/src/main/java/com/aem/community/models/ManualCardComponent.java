package com.aem.community.models;

import com.adobe.cq.sightly.WCMUsePojo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class ManualCardComponent extends WCMUsePojo
{

    private ManualCard manualCard = null;

    /** Default log. */
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void activate() throws Exception {

        manualCard = new ManualCard();


        String heading = getProperties().get("jcr:title", "");
        String description = getProperties().get("jcr:description","");
        String link = getProperties().get("jcr:link", "");

        manualCard.setTitle(heading);
        manualCard.setDescription(description);
        manualCard.setLink(link);

    }


    public ManualCard ManualCard() {
        return this.manualCard;
    }
}